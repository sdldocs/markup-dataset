# Marking up your dataset with DCAT

DCAT(Data Catalog Vocabulary)는 데이터세트에 대한 메타데이터를 컴퓨터로 읽을 수 있도록 웹에 공개하는 표준을 정의하고 있다.

데이터세트에 대한 설명을 게시하는 가장 간단한 방법은 [RDFa](?)를 사용하여 DCAT 메타데이터를 게시하는 것이다. RDFa를 사용하면 컴퓨터에서 읽을 수 있는 메타데이터를 웹 페이지에 포함할 수 있다. 즉, 데이터세트 홈페이지에 대한 HTML을 업데이트하면 데이터세트 메타데이터를 쉽게 게시할 수 있다.

이 가이드에서는 RDFa를 사용하여 DCAT 메타데이터를 게시하는 방법을 간략히 소개한다. 다른 형식의 데이터 게시를 포함하여 보다 고급 사용 사례에 대해서는 [DCAT의 공식 W3C 문서](?)를 참조하도록 한다. [RDFa Primer](?) 또한 유용한 참고 문서이다.

[개방형 데이터 인증서](?) 어플리케이션은 RDFa로 게시된 DCAT를 지원한다. 따라서 DCAT를 사용하면 데이터 소비자에게 컴퓨터로 읽을 수 있는 메타데이터를 제공할 뿐만 아니라, 어플리케이션이 일부 답변을 자동으로 생성할 수 있으므로 데이터세트 인증 프로세스를 간소화할 수 있다.

## 시작

먼저 해야 할 일은 웹페이지가 데이터세트를 기술하고 있다는 것을 어플리케이션에 알리는 것이다. 이를 위해서는 데이터세트를 기술하는 데 사용할 메타데이터 스키마를 선언한 다음 기술하는 것의 유형을 표시해야 한다.

다음은 시작을 제공하는 HTML의 일부분이다. `{url}`을 공개할 데이터세트 페이지의 URL로 변경한다.

```html
<html prefix="dct: http://purl.org/dc/terms/
              rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns#
              dcat: http://www.w3.org/ns/dcat#
              foaf: http://xmlns.com/foaf/0.1/">
    <body>
        <div typeof="dcat:Dataset" resource="{url}">
        ...
        </div>
    </body>
</html>
```

html 요소의 `prefix` 속성으로 scheme을 선언할 수 있다. `div` 요소는 리소스 및 속성 유형을 사용하여 기술할 리소스와 리소스 유형을 선언한다.

그런 다음 데이터 집합에 대한 나머지 메타데이터를 이 `<div>` 컨테이너내에 HTML 요소로 추가한다. 꼭 `div` 요소를 사용할 필요는 없으며, HTML 요소일 수 있으므로 이를 데이터세트 페이지 구조에 맞도록 조정한다.

다음 섹션 각각에서는 데이터세트를 구체적으로 기술하는 메타데이터 요소를 추가하는 방법에 대해 설명한다. 데이터세트에 대한 자세한 설명을 가능한 한 제공하도록 하여야 한다.

다시 말하지만, 예에서 사용되는 HTML 요소는 한 예일 뿐이며 원하는 것이 무엇이든 될 수 있으므로 예를 기존 페이지에 맞게 조정하여야 한다. 이미 페이지에 있는 날짜, 제목 등을 일반 텍스트로 묶기 위해 추가 요소를 추가해야 할 수도 있다.

예의 중요한 부분은 RDFa 속성인 `about`, `property`, `contents`, `datatype` 등 이다. 이러한 속성은 기술하는 데이터세트의 특성을 정의하고 컴퓨터가 읽을 수 있는 메타데이터를 제공한다.

### 제목

`dct:title` property를 사용하여 데이터세트의 제목을 지정한다.

```html
<h1 property="dct:title">Example Dataset</h1>
```

### 생성일

`dct:created` property를 사용하여 데이터세트를 만든 날짜를 지정한다.

``` html
<p property="dct:created" content='2010-10-25T09:00:00+00:00' datatype='xsd:dateTime'>25th October 2010</p>
```

위의 경우 사용자가 읽을 수 있는 property 텍스트가 `<p>` 태그 내에 포함되도록 한다. 값은 임의로 지정할 수 있지만 (콘텐츠 속성에 지정된) 컴퓨터가 읽을 수 있어 쉽게 구문 분석할 수 있도록 날짜는 정의된 데이터 유형을 사용해야 한다.

[XML schema date](http://www.w3.org/TR/xmlschema-2/#date) 또는 [XML schema dateTime](http://www.w3.org/TR/xmlschema-2/#dateTime) 형식을 사용하는 것이 바람직하다.

### 변경 시간(Date modified)

`dct:modified` 속성을 사용하여 데이터세트가 마지막으로 업데이트된 날짜를 명시핳 수 있다.

``` html
<p property="dct:modified" content='2010-10-25T09:00:00+00:00' datatype='xsd:dateTime'>25th October 2010</p>
```

날짜 형식에 대하여는 위 항목을 참조하시오.

### 설명

`dct:description` 속성을 사용하여 데이터세트을 설명한다.

``` html
<p property="dct:description">This is the description.<p>
```

### 라이센스

데이터세트 라이센스를 선언하기 위한 마크업은 약간 더 복잡합니다. 라이센스 속성(`dct:license`)과 라이센스 이름 및 URL을 선언해야 한다.

데이터세트에 적용되는 값으로 `{license URL}` 및 `{license name}` 위치를 표시하는 것으로 대체한다.

``` html
<div property="dct:license" 
     resource="{license URL}">
  <a href="{license URL}">
    <span property="dct:title">{license name}</span>
  </a>
</div>
```

[개방 데이터 권한 정보 어휘 게시자 가이드(Publishers Guide to the Open Data Rights Statement Vocabulary)](http://theodi.org/guides/publishers-guide-to-the-open-data-rights-statement-vocabulary)에서 라이센스, 저작권 정보 및 선호되는 속성 형식을 포함하는 데이터세트에 대한 포괄적인 권한 정보 게시에 대한 자세한 내용을 참조하시오.

### 발표자 (publisher)

`dct:publisher` 속성을 사용하여 데이터세트의 게시자를 선언한다. 다시 말하지만 여기에는 게시자의 이름과 홈페이지 URL을 포함하여 선언해야 할 몇 가지 요소가 있다.

`publisher URL`과 `publisher name` 속성을 적절한 값으로 바꾸어야 한다.

``` html
<div property="dct:publisher" 
     resource="{publisher URL}">
    <a href="{publisher URL}" about="{publisher URL}" property="foaf:homepage">
        <span property="foaf:name">{publisher name}</span>
    </a>
</div>
```

### 키워드

`dcat:keyword` 속성을 사용하여 데이터세트에 키워드를 첨부할 수 있다. 속성 값은 단순 레이블 또는 태그이다. 원하는 만큼 선언할 수 있다.

``` html
<span property="dcat:keyword">Examples</span>, <span property="dcat:keyword">DCAT</span>
```

### 업데이트 빈도

데이터세트가 업데이트되는 빈도를 정의하는 데 `dcat:accrucalPeriodicity` 속성을 사용다. 속성 값은 간단한 제한된 어휘를 사용하는 URI이다.

``` html
<a href="{frequency}" property="dcat:accrualPeriodicity">{frequency (human readable)}</a>
```

`{frequency}` 자리에 다음 URI중 하나를 선택하여 대체한다.

* `http://purl.org/linked-data/sdmx/2009/code#freq-A` – Annual
* `http://purl.org/linked-data/sdmx/2009/code#freq-B` – Every working day (Mon – Fri)
* `http://purl.org/linked-data/sdmx/2009/code#freq-D` – Daily (7 days a week)
* `http://purl.org/linked-data/sdmx/2009/code#freq-M` – Monthly
* `http://purl.org/linked-data/sdmx/2009/code#freq-N` – Every minute
* `http://purl.org/linked-data/sdmx/2009/code#freq-Q` – Every quarter
* `http://purl.org/linked-data/sdmx/2009/code#freq-S` – Half yearly
* `http://purl.org/linked-data/sdmx/2009/code#freq-W` – Weekly

### 배포

많은 경로로 데이터세트를 배포할 수 있다. [배포(dustribution)](http://www.w3.org/TR/vocab-dcat/#class-distribution)는 데이터세트의 패키징과 릴리스 방식을 설명한다. 또한 여러 방법으로 데이터세트를 배포할 수 있다. 예를 들어, 일정 기간 동안 일련의 데이터를 별도의 패키지로 게시하거나 다른 형식 사용할 수 있는 경우이다.

이를 위한 마크업은 조금 더 복잡하다. 새 리소스(`dcat:Distribution`)를 데이터 세트와 연결하는 것이다. 그런 다음 내재된(nested) 마크업은 새로운 리소스에 대한 메타데이터(예: 형식, 크기, 게시 날짜 등)를 제공한다.

``` html
<div property='dcat:distribution' typeof='dcat:Distribution'>
    <span property="dct:title">{Distribution title}</span>
    <ul>
        <li><strong>Format</strong> <span content='{format}' property='dcat:mediaType'>{format (human readable)</span></li>
        <li><strong>Size</strong> <span content='{size in bytes}' datatype='xsd:decimal' property='dcat:byteSize'>{size (human readable)}</span></li>
        <li><strong>Issued</strong> <span property='dct:issued' content='{date issued}' datatype='xsd:date'>{date issued (human readable)}</span></li>
    </ul>
    <p><a href='{link to data}' property='dcat:accessURL'>Download the full dataset</a></p>
</div>
```

`{format}`은 `text/csv` 또는 ` application/json`와 같은 [알려진 MIME 유형](http://www.iana.org/assignments/media-types/media-types.xhtml)이어야 한다 .

`dct:issued` 속성은 배포가 게시된 날짜를 지정한다. 속성은 위에서 설명한 `dct:created` 속성과 동일한 지침을 따른다.

## 통합

다음은 DCAT를 사용하여 표시한 HTML 페이지의 전체 예이다. 단일 배포에 대한 설명을 포함하여 데이터세트에 대한 모든 핵심 메타데이터를 제공하고 있다.

``` html
<!DOCTYPE html>
<html prefix="dct: http://purl.org/dc/terms/
              rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns#
              dcat: http://www.w3.org/ns/dcat#
              foaf: http://xmlns.com/foaf/0.1/">
    <head>
        <title>DCAT in RDFa</title>
    </head>
 <body>
 <div typeof="dcat:Dataset" resource="http://gov.example.org/dataset/finances">
            <h1 property="dct:title">Example DCAT Dataset</h1> 

              <p property="dct:created" content='2010-10-25T09:00:00+00:00' datatype='xsd:dateTime'>25th October 2010</p>
              <p property="dct:modified" content='2013-05-10T13:39:36+00:00' datatype='xsd:dateTime'>10th March 2013</p>

              <p property="dct:description">This is the description.<p>

            <div property="dct:license" 
                 resource="http://reference.data.gov.uk/id/open-government-licence">
    <a href="http://reference.data.gov.uk/id/open-government-licence">
                <span property="dct:title">UK Open Government Licence (OGL)</span>
                </a>
            </div>

            <div property="dct:publisher" 
                 resource="http://example.org/publisher">
                <a href="http://example.org/publisher" about="http://example.org/publisher" property="foaf:homepage">
                    <span property="foaf:name">Example Publisher</span>
                </a>
            </div>

            <div>
                <span property="dcat:keyword">Examples</span>, <span property="dcat:keyword">DCAT</span>
            </div>

            <div>
                <a href="http://purl.org/linked-data/sdmx/2009/code#freq-W" property="dcat:accrualPeriodicity">Weekly</a>
            </div>

      <div property='dcat:distribution' typeof='dcat:Distribution'>
          <span property="dct:title">CSV download</span>
          <ul>
              <li><strong>Format</strong> <span content='text/csv' property='dcat:mediaType'>CSV</span></li>
              <li><strong>Size</strong> <span content='240585277' datatype='xsd:decimal' property='dcat:byteSize'>1024MB</span></li>
              <li><strong>Issues</strong> <span property='dct:issued'>2012-01-01</span></li>
          </ul>
          <p><a class='btn btn-primary' href='http://example.org/distribution.csv.zip' property='dcat:accessURL'>Download the full dataset</a></p>
      </div>

    </body>
</html>
```
